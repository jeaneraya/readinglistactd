//1. Create a readingListActD folder. Inside create an index.html and index.js files for the first part of the activity and create a crud.js file for the second part of the activity.
//2. Once done with your solution, create a repo named 'readingListActD' and push your documents.
//3. Save the repo link on S32-C1

//Part 1:
/*
Create a readingListActD folder. Inside, create an index.html and index.js file. Test the connection of your js file to the html file by printing 'Hello World' in the console.
1.)

Create a student class sectioning system based on their entrance exam score.
If the student average is from 80 and below. Message: Your section is Grade 10 Section Ruby,
If the student average is from 81-120. Message: Your section is Grade 10 Section Opal,
If the student average is from 121-160. Message: Your section is Grade 10 Section Sapphire,
If the student average is from 161-200 to. Message: Your section is Grade 10 Section Diamond

Sample output in the console: Your score is (score). You will become proceed to Grade 10 (section)
*/

console.log("Hello World");

function grade(grade){
  if (grade <= 80){
    console.log(`Your score is ${grade}. Your section is Grade 10 Section Ruby`);
  } else if(grade >= 81 && grade <= 120) {
    console.log(`Your score is ${grade}. Your section is Grade 10 Section Opal`);
  } else if(grade >= 121 && grade <= 160) {
    console.log(`Your score is ${grade}. Your section is Grade 10 Section Sapphire`);
  } else if(grade >= 161 && grade <= 200) {
    console.log(`Your score is ${grade}. Your section is Grade 10 Section Ruby`);
  }
}

grade(80);

/*2.) 
Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.

Sample Data and output:
Example string: 'Web Development Tutorial'
Expected Output: 'Development'
*/


function findLongestWord(string) {
  let sentence = string.split(' ');
  let longestWord = 0;
  let currentLongest;
  for(let i = 0; i < sentence.length; i++){
    if(sentence[i].length > longestWord){
  longestWord = sentence[i].length;
  currentLongest = sentence[i];
     }
  }
  console.log(currentLongest);
}
findLongestWord("Web Development Tutorial");

/*
3.)
Write a JavaScript function to find the first not repeated character.

Sample arguments : 'abacddbec'
Expected output : 'e'

*/

function nonReapeatedChar(string) {
  let stringArray = string.split('');
  let result = '';
 
  for (let i = 0; i < stringArray.length; i++) {
      let num = 0;
    for (let a = 0; a < stringArray.length; a++) 
    {
      if (stringArray[i] === stringArray[a]) {
        num+= 1;
      }
    }
 
    if (num < 2) {
      result = stringArray[i];
    }
  }
  return result;
}
console.log(nonReapeatedChar('abacddbec'));

//Part 2:
/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots

*/

